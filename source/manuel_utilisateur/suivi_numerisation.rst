.. _suivi_numerisation:

Suivi de la numérisation
########################

Le principe
===========

Le menu de numérisation permet le suivi de la numérisation des documents des dossiers d'instruction, réalisé par la cellule de numérisation.

Pour utiliser cette fonctionnalité, il suffit d'activer l'option :ref:`option_suivi_numerisation<administration_liste_options>`.
Il est également nécessaire de définir les :ref:`paramètres numerisation_type_dossier_autorisation et numerisation_intervalle_date<parametrage_parametre_suivi_numerisation>`.
En cas de multi-collectivcité, les traitements par lot sur toutes les collectivités en même temps, ne sont possibles que lorsque les paramètres sont exclusivement définit sur la collectivité référencée comme multi.

La liste des points suivants détails chaque étape à réaliser pour le suivi de numérisation des dossiers d'instruction.

.. note::
    Dans chaque interface, la mention à la colectivité est disponible seulement pour les utilisateurs de la collectivité référencé comme multi.

.. _suivi_numerisation_recuperation:

Récupération du suivi des dossiers d'instruction
================================================

(:menuselection:`Numérisation --> Traitement d'un lot --> Récupération du suivi des dossiers`)

Cette interface présente l'action permetant la récupération des dossiers d'instruction dont le type est renseigné dans le paramètre :ref:`numerisation_type_dossier_autorisation<parametrage_parametre_suivi_numerisation>` et dont la date de dépôt se situe entre la date du jour moins l'intervalle de jour renseigné dans le paramètre :ref:`numerisation_type_dossier_autorisation<parametrage_parametre_suivi_numerisation>`. L'action peut-être réalisée manuellement depuis l'interface ou être déclenchée par un :ref:`webservice<web_services_ressource_maintenance_suivi_numerisation>`.

.. image:: suivi_numerisation_num_dossier_recuperation.png

Lorsque le traitement est terminé, un message indique à l'utilisateur le nombre de dossiers d'instruction dont le suivi a été créé.


Création du bordereau de suivi
==============================

(:menuselection:`Numérisation --> Traitement d'un lot --> Tous les bordereaux`)

Cette interface liste tous les bordereaux de suivi.

Il est possible d'ajouter un bordereau de suivi depuis cette interface en cliquant sur le "+" dans le coin supérieur gauche du tableau.

.. image:: suivi_numerisation_num_bordereau_tab.png

Lors de l'ajout du bordereau, la date à renseigner est celle de l'envoi à la cellule de numérisation.

.. image:: suivi_numerisation_num_bordereau_form_ajout.png


Associer des suivi de dossiers à un bordereau
=============================================

(:menuselection:`Numérisation --> suivi dossier --> Dossiers à attribuer`)

Lorsque la récupération de dossiers est faite, il est désormais nécessaire d’associer un suivi de dossier à un bordereau.
La liste des suivis de dossier qui ne sont pas encore associés à un bordereau s’affiche.

.. image:: suivi_numerisation_num_dossier_a_attribuer_tab.png

Depuis le tableau, sélectionnez le suivi de dossier et choisissez le bordereau sur lequel l'associer.

.. image:: suivi_numerisation_num_dossier_a_attribuer_form_modif_bordereau.png


Transmettre un bordereau à la cellule numérisation
==================================================

(:menuselection:`Numérisation --> Traitement d'un lot --> Tous les bordereaux`)

En sélectionnant un bordereau de suivi, l'action "Édition" permet de générer le document PDF à transmettre à la cellule suivi.

.. image:: suivi_numerisation_num_bordereau_form_consult_edition.png

Il est également possible de consulter l'édtion PDF du bordereau directement depuis le tableau.

.. image:: suivi_numerisation_num_bordereau_tab_edition.png


Retour du bordereau de la cellule de numérisation par lot
=========================================================

(:menuselection:`Numérisation --> Traitement d'un lot --> Tous les bordereaux`)

En sélectionnant un bordereau de suivi, l'action "Retour de la numérisation" permet de renseigner automatiquement à la date du jour, la date de retour de numérisation sur chaque suivi de dossier associé au bordereau.

.. image:: suivi_numerisation_num_bordereau_form_consult_retournum.png

L'onglet "Suivi des dossiers du bordereau" liste tous les suivis de dossier associés au bordereau. La colonne "date de numérisation" permet de contrôler le bon fonctionnement de l'action de retour de la numérisation.

.. image:: suivi_numerisation_num_bordereau_form_onglet_num_dossier.png


Retour du bordereau de la cellule de numérisation par suivi
===========================================================

(:menuselection:`Numérisation --> Suivi dossier --> Dossiers à numériser`)

Cett interface liste tous les suivi de dossier à numériser, c'est-à-dire qu'ils sont affectés à un bordereau mais n'ont pas encore de date de numérisation.

.. image:: suivi_numerisation_num_dossier_a_numeriser_tab.png

Il est possible de mettre à jour la date de retour de numérisation depuis le suivi d'un dossier.

.. image:: suivi_numerisation_num_dossier_a_numeriser_form_datenum.png


Modifier les caractéristiques d’un suivi de dossier d'instruction numérisé
==========================================================================

(:menuselection:`Numérisation --> Suivi dossier --> Dossiers traités`)

Depuis la liste des suivi de dossier traités, il est possible de modifier les caractéristiques d'un enregistrement en le sélectionnant.

.. image:: suivi_numerisation_num_dossier_traite_tab.png

.. image:: suivi_numerisation_num_dossier_traite_form_pages.png
